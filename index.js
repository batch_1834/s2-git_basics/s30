// Include express module
const express = require("express");

// Mongoose is a package that allows creation of Schemas to model data structure.
// Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose");

// Setup the express server
const app = express();

// List the port where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to the database by passing in your connection string
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>",
		{useNewUrlParser: true, useUnifiedTopology: true});


*/
// connection string from mongodb
mongoose.connect("mongodb+srv://admin:admin@coursebooking.lupvtnf.mongodb.net/b183_to-do?retryWrites=true&w=majority",{useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our databse.
let db = mongoose.connection;

// Id a connection error occured, it will be output in the console.
// console.error.bind(console) allows us to print the error in the browser consoles and in the terminal.
db.on("error", console.error.bind(console,"connection error"))

// If the connectiong is successful, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud database."))

// Mongoose Schema
// It will determine the structure of the document to be written in the database.
// Schemas act as blueprints to our data.
const taskSchema = new mongoose.Schema({
	// Name of the task
	 name: String, //String is a shorthand for name:{type: String}
	 // Status task (Complete, pending, Incomplete)
	 status: {
	 	type: String,
	 	// Default values are the predefined values for a field if we don't put any value.
	 	default: "Pending"
	 }
})

// Mongoose Model
// Model uses Schemas and they act as the middleman from the server (JS code) to our database.

/*
	Syntax:
		const modelName = mongoose.model("collectionName", mongooseSchema);

*/

// Model must be in singular form and capitalize the first letter.
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection.
const Task = mongoose.model("Task", taskSchema);


// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Route for Creating a task
// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is not duplicate if:
	- result from the query not equal to null.
	- req.nody.name equal to result.name

*/

app.post("/tasks", (req, res) =>{
	// Call back functions in mongoose methods are programmed this way:
	// 	first parameter store the error
	// second parameter return the result
	// req.body.name = eat
	// Task.findOne({name:eat})
		Task.findOne({name:req.body.name}, (err, result)=>{
			console.log(result)

			// If a document was found and the document's name mathces the information sent client/postman.
			if(result != null && req.body.name == result.name){
				// Return a message to the client/postman
				return res.send("Duplicate task found")
			}
			// If no document was found or no duplicate.
			else{
				// Create a new task and save to database

				let newTask = new Task({
					name: req.body.name
				});

				// The ".save" method will store the information to the database
				// Since the "newTask" was created/instantiated from the Task model that contains the Mongoose Schema, so it will gain access to the save method.

				newTask.save((saveErr, saveTask) =>{
					if(saveErr){
						return console.error(saveErr);
					}
					// If no error found while creating the document it will be save in the database
					else{
						return res.status(201).send("New task created.")
					}
				})

			}
	})

})
// Business Logic
		/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client/Postman and return an array of documents
		*/

// app.get("/tasks",(req, res)=>{
// 	Task.find({}, (err, result)=>{
// 		if(err){
// 			return console.log(err);
// 		}
// 		else{
// 			return res.status(200).send(result);
// 		}
// 	})
// })

// Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.

*/

//Register a user

/*
	Business Logic:

	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
		- Note: Make sure that the username and password from the request is not empty.
	3. Create a new User object with a "username" and "password" fields/properties
*/

const userSchema = new mongoose.Schema({
	userName: String,
	passWord: String,

	status:{
		type:String,
		default: "Pending"
	}
})

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) =>{

	User.findOne({username:req.body.username},{ password:req.body.password},(err, result)=>{
		console.log(result)

		if(result != null && req.body.username == result.username){
			return res.send("Duplicate user found")
		}
		else if(result != null && req.body.password == result.password){
			return res.send("Duplicate password found")
		}
		else{

			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveUser) =>{

				if(saveErr){
					return console.log(saveErr);
				}
				else{
					return res.status(201).send("New user created.");
				}
			})

		}
	})
})

app.get("/registered",(req, res)=>{
	User.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result)
		}
	})
})





// Listen to the port
app.listen(port, () =>console.log(`Server running at port ${port}`));



